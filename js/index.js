		$(function(){
			$("[data-bs-toggle='tooltip']").tooltip();
			$("[data-bs-toggle='popover']").popover();
			$('.carousel').carousel({
				interval: 1000});
			$("#contacto").on("show.bs.modal", function(e){
				console.log("el modal contacto se está mostrando");
				$("#contactoBtn").prop("disabled", true);
			});
			$("#contacto").on("hidden.bs.modal", function(e){
				console.log("el modal se ocultó");
				$("#contactoBtn").prop("disabled", false);
			});

		});